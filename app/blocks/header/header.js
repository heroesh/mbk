$(document).ready(function () {
	//burger
	$('.js-burger').click(function () {
		$('.js-nav').toggleClass('active');
		$(this).toggleClass('active');
		$('body').toggleClass('overflow');
	});

	//modals
	$(".js-open-order").click(function () {
		$(".js-order-modal").fadeIn();
		$(".js-mask").fadeIn();
		$('body').addClass('overflow')
	});

	$(".js-open-thx").click(function () {
		$(".js-thx-modal").fadeIn();
		$(".js-order-modal").fadeOut();
	});

	//modalsclose

	$(".js-mask, .js-cancel").click(function () {
		$(".js-modal").fadeOut();
		$(".js-mask").fadeOut();
		$('body').removeClass('overflow')
	});
});
