//=require ../blocks/**/*.js
$(document).ready(function () {
  
  AOS.init({
    disable: function() {
      var maxWidth = 1000;
      return window.innerWidth < maxWidth;
    }
  });

  $(window).scroll(function(){
    var sticky = $('.sticky'),
        scroll = $(window).scrollTop();
  
    if (scroll >= 100) sticky.addClass('fixed');
    else sticky.removeClass('fixed');
  });

  $(document).on('click', '.scroll-link', function(e) {
    // target element id
    var id = $(this).attr('href');

    // target element
    var $id = $(id);
    if ($id.length === 0) {
        return;
    }

    // prevent standard hash navigation (avoid blinking in IE)
    e.preventDefault();

    // top position relative to the document
    var pos = $id.offset().top;

    // animated top scrolling
    $('body, html').animate({scrollTop: pos});
});
  //fullpage
  // if (window.matchMedia("(min-width: 1024px)").matches) {
  //   new fullpage("#fullpage", {
  //     anchors: [
  //       "intro",
  //       "about",
  //       "work",
  //       "founder",
  //       "build",
  //       "trust",
  //       "task",
  //       "figures",
  //       "values",
  //       "partners",
  //     ],
  //     navigation: true,
  //     scrollingSpeed: 1200,
  //     loopTop: true,
  //     loopBottom: true,
  //     scrollOverflow: true,
  //     navigationTooltips: ['Головна', 'Про нас', 'Як ми працюємо', 'Чому ми будуємо', 'Портфоліо', 'Відгуки', 'Задача', 'Цифри', 'Цінності', 'Партнери'],
  //   });
  // } else {
  //   fullpage_api.destroy();
  // }

  //slider
  var introSlider = new Swiper(".js-intro-slider", {
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
    pagination: {
      el: ".swiper-pagination",
    },
  });

  var buildSlider = new Swiper(".js-build-slider", {
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
    pagination: {
      el: ".swiper-pagination",
    },
  });

  var workSlider = new Swiper(".js-work-slider", {
    spaceBetween: 20,
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
    pagination: {
      el: ".swiper-pagination",
    },
  });

  var trustSlider = new Swiper(".js-trust-slider", {
    spaceBetween: 20,
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
    pagination: {
      el: ".swiper-pagination",
    },
  });

  var partnerSlider = new Swiper(".js-partner-slider", {
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
    pagination: {
      el: ".swiper-pagination",
    },
    breakpoints: {
      499: {
        slidesPerView: 1
      },
      768: {
        slidesPerView: 2

      },
      1024: {
        slidesPerView: 3
      },
      1440: {
        slidesPerView: 4
      },
      1920: {
        slidesPerView: 5
      }
    },
  });

  $('.js-build-show-more').click(function () {
    $('.js-build-slider').show();
    $(this).hide();
    var buildSlider = new Swiper(".js-build-slider", {
      navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
      },
      pagination: {
        el: ".swiper-pagination",
      },
    });
    // fullpage_api.reBuild();
  });

  //anim
  if ($(window).width() < 1200) {
    $(window).scroll(function () {
      $('.mov').each(function () {
        var imagePos = $(this).offset().top;
        var topOfWindow = $(window).scrollTop();

        if (imagePos < topOfWindow + 600) {
          $(this).addClass('anim');
        }
      });
    });
  };

  //scroll to section

  $(document).on('click', '.js-scroll-link-parent a', function (e) {
    $('body').removeClass('overflow');
    $('.js-nav').removeClass('active');
    $('.js-burger').removeClass('active');
    var id = $(this).attr('href');

    var $id = $(id);
    if ($id.length === 0) {
      return;
    }

    e.preventDefault();

    var pos = $id.offset().top;

    $('body, html').animate({ scrollTop: pos });
  });
});
